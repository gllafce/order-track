import "bootstrap/dist/css/bootstrap.min.css";
import Container from "react-bootstrap/Container";
import Header from "./components/Header";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import Home from "./components/pages/home"
import OrderDetails from "./components/pages/orderDetails";

function App() {
  return (
    <Router>
      <Container fluid className="px-0 app-container">
        <Header />
        <Switch>
            <Route path="/" exact render={Home} />
            <Route path="/order/:id" render={OrderDetails} />
        </Switch>
      </Container>
    </Router>
  );
}

export default App;
