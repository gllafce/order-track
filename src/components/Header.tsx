import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { Button } from "react-bootstrap";
import { PlusSquare } from "react-bootstrap-icons";
import { Link } from "react-router-dom";

function Header() {
  return (
    <Container fluid className="px-5 border app-header">
      <Row className="p-5">
        <Col>
          <Link className="text-decoration-none" to="/">
            <PlusSquare color="#44aaaa" size={96} />
          </Link>
        </Col>
        <Col className="text-right pr-5">
          <Button className="mr-5 btn bg-transparent py-3 px-5 text-dark rounded-pill">
            Contact Us
          </Button>
        </Col>
      </Row>
    </Container>
  );
}
export default Header;
