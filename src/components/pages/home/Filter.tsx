import { useContext, useState } from "react";
import Col from "react-bootstrap/esm/Col";
import Row from "react-bootstrap/esm/Row";
import useEffectExceptOnMount from "../../../hooks/useEffectExceptOnMount";
import { FilterContext } from "../../../state/filters";
import { Types } from "../../../state/filters/actions";
import DropDownFilter from "../../shared/DropDownFilter";

function Filter({sendApiRequest}: {sendApiRequest: Function}) {
  const { state, dispatch } = useContext(FilterContext);
  const [statusFilterState, setStatusFilterState] = useState(
    state.statusFilter
  );
  const [dateFilterState, setDateFilterState] = useState(state.dateFilter);

  useEffectExceptOnMount(() => {
    dispatch({
      type: Types.select_filter,
      payload: { name: "Status", selected: statusFilterState.selected },
    });
    sendApiRequest()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [statusFilterState]);

  useEffectExceptOnMount(() => {
    dispatch({
      type: Types.select_filter,
      payload: { name: "Date", selected: dateFilterState.selected },
    });
    sendApiRequest()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dateFilterState]);

  return (
    <Row className="filters pt-5 app-filter">
      <Col sm="7">
        <h3 className="pb-3 pt-3 app-filter-title">Active</h3>
      </Col>
      <Col sm="5">
        <Row>
          <Col>
            <DropDownFilter
              val={statusFilterState}
              onSelected={setStatusFilterState}
            />
          </Col>
          <Col>
            <DropDownFilter
              val={dateFilterState}
              onSelected={setDateFilterState}
            />
          </Col>
        </Row>
      </Col>
    </Row>
  );
}

export default Filter;
