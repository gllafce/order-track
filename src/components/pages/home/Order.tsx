import Col from "react-bootstrap/esm/Col";
import Row from "react-bootstrap/esm/Row";
import { Link } from "react-router-dom";
import IOrder from "../../../interfaces/IOrder";


function Order({ order }: { order: IOrder }) {
  const { id, patient, payment, items, status, date } = order;
  return (
    <Row className="pt-4 pb-4 border-bottom app-order">
      <Col>
        <Row>
          <Col>
            <Link className="text-decoration-none" to={`order/${id}`}>
              {patient.name}
            </Link>
          </Col>
          <Col>{payment}</Col>
          <Col>{items}</Col>
          <Col>{status}</Col>
          <Col>{date}</Col>
        </Row>
      </Col>
    </Row>
  );
}

export default Order;
