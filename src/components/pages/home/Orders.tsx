import { useContext, useEffect } from "react";
import useFetchOrders from "../../../hooks/useFetchOrders";
import { OrdersContext } from "../../../state/orders";
import { Types } from "../../../state/orders/actions";
import Filter from "./Filter";
import Order from "./Order";
import TableHeader from "./TableHeader";

function Orders() {
  const { data, fetchData } = useFetchOrders();
  const { state, dispatch } = useContext(OrdersContext);

  const handleFilter = () => {
    dispatch({ type: Types.clear });
    fetchData();
  };

  useEffect(() => {
    if (data) dispatch({ type: Types.fetch, payload: data });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  return (
    <>
      <Filter sendApiRequest={handleFilter} />
      <TableHeader />
      {state.orders.map((order) => {
        return <Order key={order.id} order={order} />;
      })}
    </>
  );
}

export default Orders;
