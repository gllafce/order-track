import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import OverviewBox, { IOverviewBox } from "../../shared/OverviewBox";
import { useEffect, useState } from "react";
import useFetch from "../../../hooks/useFetch";

function Overview() {
  const URL = `${window.location.protocol}//${window.location.host}/overview.json`;
  const { data } = useFetch<IOverviewBox[]>(URL);
  const [overViewData, setOverviewData] = useState<IOverviewBox[]>([]);

  useEffect(() => {
    if(data) setOverviewData(data);
  }, [data]);

  return (
    <Container fluid className="app-overview px-5 border">
      <Row className="py-5 text-center">
        <Col>
          <h3>Overview</h3>
        </Col>
      </Row>
      {overViewData.map((box) => {
        return (
          <Row className="pb-5" key={box.id}>
            <OverviewBox box={box} />
          </Row>
        );
      })}
    </Container>
  );
}

export default Overview;
