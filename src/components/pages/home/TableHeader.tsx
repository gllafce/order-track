import Col from "react-bootstrap/esm/Col";
import Row from "react-bootstrap/esm/Row";

function TableHeader() {
  return (
    <Row className="pt-3 pb-3 border-bottom">
      <Col>
        <Row className="font-weight-light">
          <Col>Patient Name</Col>
          <Col>Payment</Col>
          <Col>Items</Col>
          <Col>Status</Col>
          <Col>Date</Col>
        </Row>
      </Col>
    </Row>
  );
}

export default TableHeader;
