import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Orders from "./Orders";
import Overview from "./Overview";
import Title from "./Title";

function Home() {
  
  return (
    <Container fluid className="px-5 app-home">
      <Row className="p-5">
        <Col sm="9">
          <Title />
          <Orders />
        </Col>
        <Col sm="3">
          <Overview />
        </Col>
      </Row>
    </Container>
  );
}
export default Home;
