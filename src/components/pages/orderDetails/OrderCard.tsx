import {
  CardText,
  CartCheckFill,
  Truck,
  GeoAltFill,
} from "react-bootstrap-icons";
import Alert from "react-bootstrap/Alert";
import Card from "react-bootstrap/Card";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Button from "react-bootstrap/Button";
import IOrder from "../../../interfaces/IOrder";

function OrderCard({order}: {order: IOrder}) {
  return (
    <Card className="shadow">
      <Card.Body>
        <Card.Title>Current Order</Card.Title>
        
          <Alert variant="light" className="border">
            <Row>
              <Col sm="2">
                <CartCheckFill color="#e44690" size={50} />
              </Col>
              <Col sm="10">
                <Row className="font-weight-bold">{order.name}</Row>
                <Row><span>{order.description}</span></Row>
              </Col>
            </Row>
          </Alert>
          <Alert variant="light" className="border w-50">
            <Row>Quantity: {order.items} x</Row>
          </Alert>
          <Card.Title>Update Order:</Card.Title>
          <Alert variant="light">
            <Row className="app-timeline-v">
              <Col>
                <Button className="bg-transparent bg-white border-0 shadow">
                  <CardText color="#44aaaa" size={50} />
                </Button>
                <span>Dispensed</span>
              </Col>
              <Col>
                <hr />
              </Col>
              <Col>
                <Button className="bg-transparent bg-white border-0 shadow">
                  <Truck color="#44aaaa" size={50} />
                </Button>
                <span>Dispached</span>
              </Col>
              <Col>
                <hr />
              </Col>
              <Col>
                <Button className="bg-transparent bg-white border-0 shadow">
                  <GeoAltFill color="#44aaaa" size={50} />
                </Button>
                <span>Ordered</span>
              </Col>
            </Row>
          </Alert>
      </Card.Body>
    </Card>
  );
}

export default OrderCard;
