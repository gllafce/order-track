import { useEffect, useState } from "react";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { useParams } from "react-router-dom";
import useFetchOrders from "../../../hooks/useFetchOrders";
import IOrder from "../../../interfaces/IOrder";
import OrderCard from "./OrderCard";

function OrderList() {
  const { id } = useParams() as any;
  if (!id) {
    <Row>Error</Row>;
  }
  const [orderList, setOrderList] = useState<IOrder[]>([]);
  const { data } = useFetchOrders({ id: parseInt(id) });

  useEffect(() => {
    if (data) setOrderList(data);
  }, [data]);

  return (
    <Row className="app-orders">
      {orderList?.map((order, id) => {
        return (
          <Col sm="4" key={id} className="pt-4">
            <OrderCard order={order} />
          </Col>
        );
      })}
    </Row>
  );
}

export default OrderList;
