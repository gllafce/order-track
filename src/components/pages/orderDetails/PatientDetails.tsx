import { useState, useEffect } from "react";
import { PersonCircle } from "react-bootstrap-icons";
import Card from "react-bootstrap/Card";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import useFetch from "../../../hooks/useFetch";
import IPatient from "../../../interfaces/IPatient";

function PatientDetails() {
  const [patient, setPatient] = useState<IPatient>()
  const URL = `${window.location.protocol}//${window.location.host}/profile.json`;
  const { data } = useFetch<IPatient>(URL);

  useEffect(() => {
    if (data) setPatient(data)
  }, [data])

  return (
    <Row className="app-personal-info">
      <Col>
        <Card>
          <Row>
            <Col className="app-avatar">
              <PersonCircle color="#44aaaa" size={200} />
            </Col>
            <Col>
              <Row className="app-name py-3">{patient?.name}</Row>
              <Row>{patient?.birthday}</Row>
              <Row>{patient?.gender}</Row>
            </Col>
            <Col>
              <Row className="app-title py-3">Address</Row>
              <Row>{patient?.address}</Row>
            </Col>
            <Col>
              <Row className="app-title py-3">Exemption</Row>
              <Row>{patient?.exemption}</Row>
            </Col>
          </Row>
        </Card>
      </Col>
    </Row>
  );
}

export default PatientDetails;
