import Container from "react-bootstrap/Container";
import OrderList from "./OrderList";
import PatientDetails from "./PatientDetails";

function OrderDetails() {
  return (
    <Container fluid className="app-order-details">
      <PatientDetails />
      <OrderList />
    </Container>
  );
}

export default OrderDetails;
