import Dropdown from "react-bootstrap/Dropdown";

export interface IDropDownFilter {
  name: string;
  values: Array<string>;
  selected: number;
}

function DropDownFilter({
  val,
  onSelected,
}: {
  val: IDropDownFilter;
  onSelected: Function;
}) {
  const onSelect = (id: number) => {
    val = {
      ...val,
      selected: id,
    };
    onSelected(val);
  };

  return (
    <Dropdown className="app-dropdown rounded-pill">
      <Dropdown.Toggle variant="none" className=" pt-3 pb-3  w-100 text-monospace">
        {val.name}: {val.values[val.selected]}
      </Dropdown.Toggle>
      <Dropdown.Menu>
        {val.values.map((item, id) => {
          return (
            <Dropdown.Item key={id} onClick={() => onSelect(id)}>
              {item}
            </Dropdown.Item>
          );
        })}
      </Dropdown.Menu>
    </Dropdown>
  );
}

export default DropDownFilter;
