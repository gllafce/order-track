import Alert from "react-bootstrap/Alert";

export interface IOverviewBox {
    id: number;
    title: string;
    value: string;
}

function OverviewBox({ box }: { box: IOverviewBox}) {
  return (
    <Alert className="border w-100 px-5 app-overview-box">
      <p>{box.title}</p>
      <Alert.Heading>{box.value}</Alert.Heading>
    </Alert>
  );
}

export default OverviewBox;
