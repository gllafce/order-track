import { useRef, useEffect } from "react";

function useEffectExceptOnMount (effect: Function, dependencies:  Array<any> =[])  {
  const mounted = useRef(false);
  useEffect(() => {
    if (mounted.current) {
      const unmount = effect();
      return () => unmount && unmount();
    } else {
      mounted.current = true;
    }
  }, [...dependencies]);

  // Reset on unmount for the next mount.
  useEffect(() => {
    return () => { mounted.current = false };
  }, []);
};

export default useEffectExceptOnMount;
