import axios, { AxiosRequestConfig } from "axios";
import { useEffect, useReducer, useRef } from "react";

// State & hook output
interface State<T> {
  status: "init" | "fetching" | "error" | "fetched";
  data?: T;
  error?: string;
  fetchData: Function;
}

interface Cache<T> {
  [url: string]: T;
}

// discriminated union type
type Action<T> =
  | { type: "request" }
  | { type: "success"; payload: T }
  | { type: "failure"; payload: string };

function useFetch<T = unknown>(
  url?: string,
  options?: AxiosRequestConfig
): State<T> {
  const cache = useRef<Cache<T>>({});
  const cancelRequest = useRef<boolean>(false);

  const initialState: State<T> = {
    status: "init",
    error: undefined,
    data: undefined,
    fetchData: () => {}
  };

  // Keep state logic separated
  const fetchReducer = (state: State<T>, action: Action<T>): State<T> => {
    switch (action.type) {
      case "request":
        return { ...initialState, status: "fetching" };
      case "success":
        return { ...initialState, status: "fetched", data: action.payload };
      case "failure":
        return { ...initialState, status: "error", error: action.payload };
      default:
        return state;
    }
  };

  const [state, dispatch] = useReducer(fetchReducer, initialState);

  const fetchData = async () => {
    if (!url) {
      return;
    }
    console.log("APIIII CALLL");
    dispatch({ type: "request" });

    // if (cache.current[url]) {
    //   dispatch({ type: "success", payload: cache.current[url] });
    // } else {
      try {
        const response = await axios(url, options);
        cache.current[url] = response.data;

        if (cancelRequest.current) return;

        dispatch({ type: "success", payload: response.data });
      } catch (error) {
        if (cancelRequest.current) return;

        dispatch({ type: "failure", payload: error.message });
      }
    
  };

  useEffect(() => {
    if (!url) {
      return;
    }
    fetchData();

    return () => {
      cancelRequest.current = true;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [url]);

  return {...state, fetchData};
}

export default useFetch;
