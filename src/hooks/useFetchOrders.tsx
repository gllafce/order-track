import IOrder from "../interfaces/IOrder";
import useFetch from "./useFetch";

function useFetchOrders(params: any = {}) {
  
  const URL = `${window.location.protocol}//${window.location.host}/orders.json`;
  const { data, status, error, fetchData } = useFetch<IOrder[]>(URL);

  return { data, status, error, fetchData };
}

export default useFetchOrders;
