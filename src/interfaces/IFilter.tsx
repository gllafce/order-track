import { IDropDownFilter } from "../components/shared/DropDownFilter";

export default interface IFilterState {
    statusFilter: IDropDownFilter,
    dateFilter: IDropDownFilter
}