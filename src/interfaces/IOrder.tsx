import IPatient from "./IPatient";

export default interface IOrder {
  id: number;
  name: string;
  description: string;
  patient: IPatient;
  payment: string;
  items: number;
  status: string;
  date: string;
}

export interface IOrdersState {
  orders: IOrder[];
  isFetching: boolean;
}
