export default interface IPatient {
  id: number;
  name: string;
  gender: string;
  birthday: string;
  address: string;
  exemption: string;
}
