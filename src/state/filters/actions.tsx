import IFilterState from "../../interfaces/IFilter";

export enum Types {
  select_filter = "select_filter",
  clear = "clear_all_filters",
}

export const initialState: IFilterState = {
  statusFilter: {
    name: "Status",
    values: ["All", "Dispatched", "Delivered", "To Dispense"],
    selected: 0,
  },
  dateFilter: {
    name: "Date",
    values: ["All", "Today", "Tomorrow", "Yesterday"],
    selected: 1,
  }
};

export type Action =
  | { type: Types.clear }
  | { type: Types.select_filter; payload: {name: string, selected: number} };
