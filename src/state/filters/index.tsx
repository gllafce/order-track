import { createContext, Dispatch, ReactNode, useReducer } from "react";
import IFilterState from "../../interfaces/IFilter";
import { initialState, Action } from "./actions";
import { filterReducer } from "./reducer";

export interface IContextProps {
  state: IFilterState;
  dispatch: Dispatch<Action>;
}

export const FilterContext = createContext<IContextProps>({} as IContextProps);

function FilterProvider({ children }: { children: ReactNode }) {
  const [state, dispatch] = useReducer(filterReducer, initialState);

  return (
    <FilterContext.Provider value={{ state, dispatch }}>
      {children}
    </FilterContext.Provider>
  );
}

export default FilterProvider;
