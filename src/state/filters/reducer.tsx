import IFilterState from "../../interfaces/IFilter";
import { Types, Action, initialState } from "./actions";

export const filterReducer = (
  state: IFilterState,
  action: Action
): IFilterState => {
  console.log("DISPACHEED >>>>>>>>>", action);
  switch (action.type) {
    case Types.clear:
      return { ...initialState };
    case Types.select_filter:
      const { name, selected } = action.payload;

      if (name === "Status") state.statusFilter.selected = selected;
      if (name === "Date") state.dateFilter.selected = selected;

      return {
        ...state
      };

    default:
      return state;
  }
};
