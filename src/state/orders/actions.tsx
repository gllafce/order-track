import IOrder, { IOrdersState } from "../../interfaces/IOrder";

export enum Types {
  request = "request",
  fetch = "fetch",
  clear = "clear",
}

export const initialState: IOrdersState = {
  orders: [],
  isFetching: false,
};

export type Action =
  | { type: Types.request }
  | { type: Types.clear }
  | { type: Types.fetch; payload: IOrder[] };
