import { createContext, Dispatch, ReactNode, useReducer } from "react";
import { IOrdersState } from "../../interfaces/IOrder";
import { initialState, Action } from "./actions";
import { ordersReducer } from "./reducer";

export interface IContextProps {
  state: IOrdersState;
  dispatch: Dispatch<Action>;
}

export const OrdersContext = createContext<IContextProps>({} as IContextProps);

function OrdersProvider({ children }: { children: ReactNode }) {
  const [state, dispatch] = useReducer(ordersReducer, initialState);

  return (
    <OrdersContext.Provider value={{ state, dispatch }}>
      {children}
    </OrdersContext.Provider>
  );
}

export default OrdersProvider;
