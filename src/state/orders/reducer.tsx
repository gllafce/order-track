import { IOrdersState } from "../../interfaces/IOrder";
import { Types, Action, initialState } from "./actions";

export const ordersReducer = (state: IOrdersState, action: Action): IOrdersState => {
  console.log("DISPACHEED >>>>>>>>>", action);
  switch (action.type) {
    case Types.request:
      return { ...state };
    case Types.clear:
      return { ...initialState };
    case Types.fetch:
      return {
        ...state,
        orders: action.payload
      };
    default:
      return state;
  }
};
